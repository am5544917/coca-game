<?php

namespace App\Http\Controllers\Tracks\Track1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KeHoachController extends Controller
{
    protected $pathView = "tracks.track1.kehoach.";
    public function index(Request $request)
    {
        return view($this->pathView. 'index');
    }
}
