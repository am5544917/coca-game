<?php

namespace App\Http\Controllers\Tracks\Track1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    protected $pathView = "tracks.track1.reviews.";
    public function index(Request $request)
    {
        return view($this->pathView. 'index');
    }
    public function kehoach(Request $request)
    {
        return view($this->pathView. 'kehoach');
    }
    public function kehoachdetail(Request $request) {
        $id = $request->id;
        $nextId = $id +  1;
        $backId = $id > 1 ? $id - 1 : 1;
      
      
       
        return view($this->pathView. 'kehoach.detail-'.$id,compact('nextId','backId'));
    }
    public function sanxuat(Request $request)
    {
        return view($this->pathView. 'sanxuat');
    }
    public function sanxuatdetail(Request $request) {
        $id = $request->id;
        $nextId = $id +  1;
        $backId = $id > 1 ? $id - 1 : 1;
      
        return view($this->pathView. 'sanxuat.detail-'.$id,compact('nextId','backId'));
    }
}
