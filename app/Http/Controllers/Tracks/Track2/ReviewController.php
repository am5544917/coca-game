<?php

namespace App\Http\Controllers\Tracks\Track2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    protected $pathView = "tracks.track2.reviews.";
    public function index(Request $request)
    {
        return view($this->pathView. 'index');
    }
    public function phanphoi(Request $request)
    {
        return view($this->pathView. 'phanphoi');
    }
    public function phanphoidetail(Request $request) {
        $id = $request->id;
        $nextId = $id +  1;
        $backId = $id > 1 ? $id - 1 : 1;
      
       
        return view($this->pathView. 'phanphoi.detail-'.$id,compact('nextId','backId'));
    }
    public function phattrienthitruong(Request $request)
    {
        return view($this->pathView. 'phattrienthitruong');
    }
    public function phattrienthitruongdetail(Request $request) {
        $id = $request->id;
        $nextId = $id +  1;
        $backId = $id > 1 ? $id - 1 : 1;
      
        return view($this->pathView. 'phattrienthitruong.detail-'.$id,compact('nextId','backId'));
    }
}
