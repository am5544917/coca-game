<?php

namespace App\Http\Controllers\Tracks\Track3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    protected $pathView = "tracks.track3.reviews.";
    public function index(Request $request)
    {
        return view($this->pathView. 'index');
    }
    public function hotro(Request $request)
    {
        return view($this->pathView. 'hotro');
    }
    public function hotrodetail(Request $request) {
        $id = $request->id;
        $nextId = $id +  1;
        $backId = $id > 1 ? $id - 1 : 1;
      
       
        return view($this->pathView. 'hotro.detail-'.$id,compact('nextId','backId'));
    }
}
