<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=1200px">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Review </title>
    <link rel="stylesheet" href="{{ asset('css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}?ver={{ time() }}">
</head>

<body>
    <main class="review-main">
        <div class="review-main-content">
            @yield('content')
        </div>
    

    </main>
    <div class="bg-review @yield('track_number')"></div>

</body>

</html>
