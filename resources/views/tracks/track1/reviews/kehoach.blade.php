@extends('layouts/review')
@section('track_number', 'track-2')
@section('content')
    <div class="box-title">
        <p> Để có thể chinh phục các thử thách tại Thị Trấn một cách xuất sắc nhất và trở thành những Người dẫn đầu Tương
            lai – Future Shapers, hãy cùng tìm hiểu về khâu đầu tiên cực kỳ quan trọng – Lập kế hoạch được diễn ra thế nào
            nhé:
        </p>
    </div>
    <div class="rows-3 row-cards">
        <div class="row-item">
            <a href="{{route('tracks.track1.review.kehoachdetail',['id' => 1])}}"> <img src="{{ asset('img/hologram-box-1.png') }}" alt=""></a>
        </div>
        <div class="row-item">
            <a href="{{route('tracks.track1.review.kehoachdetail',['id' => 2])}}"><img src="{{ asset('img/hologram-box-2.png') }}" alt=""></a>
        </div>
        <div class="row-item">
            <a href="{{route('tracks.track1.review.kehoachdetail',['id' => 3])}}"> <img src="{{ asset('img/hologram-box-3.png') }}" alt=""></a>
        </div>
    </div>
@endsection
