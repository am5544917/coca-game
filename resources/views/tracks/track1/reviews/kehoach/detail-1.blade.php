@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-vai-tro-y-nghia.png') }}" alt="">
    </div>
    <div class="review-detail-content kehoach-detail">
        <div class="review-flex-center">
            <div class="box-label">
             <p>   Đưa ra những dự báo chính xác về doanh số thông qua sự am hiểu sâu sắc về tình hình thị trường để  từ đó lên kế hoạch sản xuất chi tiết nhằm:</p>
            </div>
            <div class="box-label-desc-wrap">
                <div class="box-label-desc">
                    <ul>
                        <li>Tối ưu hóa các nguồn lực</li>
                        <li> Đáp ứng nhu cầu khách hàng, </li>
                        <li> Đạt được lợi nhuận cao
                        </li>
                        <li>Gia tăng năng suất.</li>
                    </ul>
                </div>
            </div>
        </div>


    </div>
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track1.review.kehoach') }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track1.review.kehoachdetail', ['id' => $nextId]) }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
