@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-chuoi-gia-tri.png') }}" alt="">
    </div>
    <div class="review-detail-content kehoach-detail ">
        <div class="chuoi-gia-tri-diagram">
            <div class="step-2 text-box-desc khkd-desc">
                Chiến lược + Quản lý tăng trưởng doanh thu
            </div>
            <div class="step-2 text-box-desc khbh-desc">
                Quản lý tăng trưởng doanh thu <br>
                – Phòng Thương Mại
            </div>
            <div class="step-2 text-box-desc khsx-desc">
                Kế hoạch sản xuất
                – Chuỗi Cung Ứng
            </div>
            <div class="box-label step-1 khkd-content">
                <h3>Dữ liệu đầu vào</h3>
                <div class="box-label-content">
                    <p>-Tình hình kinh tế vĩ mô
                    </p>
                    <p> -Chiến lược kinh doanh của The Coca-Cola Company
                    </p>
                    <p> -Dự đoán biến động thị trường
                    </p>
                    <p> -Kết quả kinh doanh nội bộ
                    </p>
                    <p> -Khát vọng chiến thắng</p>
                </div>
            </div>
            <div class="box-label step-1 khbh-content">
                <h3>Dữ liệu đầu vào</h3>
                <div class="box-label-content">
                    <p>
                        -Kết quả kinh doanh công ty
                    </p>
                    <p> -Kế hoạch kinh doanh, dự báo doanh thu, kế hoạch dài hạn
                    </p>
                    <p> -Tình hình thị trường
                    </p>
                </div>
            </div>
            <div class="box-label step-1 khsx-content">
                <h3>Dữ liệu đầu vào</h3>
                <div class="box-label-content">
                    <p>
                        -Kế hoạch và dự báo số bán
                    </p>
                    <p> -Nguồn lực sẵn có
                    </p>
                    <p> -Kế hoạch bảo trì
                    </p>
                </div>
            </div>
            <div class="box-label-out-wrap">
                <div class="box-label step-1 khkd-content-out box-label-out">
                    <h3>Kết quả đầu ra</h3>
                    <div class="box-label-content">
                        <p>
                            Chiến lược kinh doanh dài hạn
                        </p>
                    </div>
                </div>
                <div class="box-label step-1 khbh-content-out box-label-out">
                    <h3>Kết quả đầu ra</h3>
                    <div class="box-label-content">
                        <p>
                            Kế hoạch và dự báo bán hàng chi tiết hằng năm, hằng tháng, hằng quý, theo từng vùng, trung tâm
                            phân
                            phối, nhà máy, hỗn hợp sản phẩm, thương hiệu, mã sản phẩm, kênh phân phối.
                        </p>
                    </div>
                </div>
                <div class="box-label step-1 khsx-content-out box-label-out">
                    <h3>Kết quả đầu ra</h3>
                    <div class="box-label-content">
                        <p>

                            -Kế hoạch sản xuất (dài hạn, hằng tháng)

                        </p>
                        <p>-Kế hoạch nguồn lực (dài hạn, hàng tháng, hàng tuần): nguyên vật liệu, nhân công, tài sảnl</p>
                    </div>
                </div>
            </div>

            <div class="box-step-title khkd">
                LÊN KẾ HOẠCH KINH DOANH
            </div>
            <div class="box-step-title khbh">
                LÊN KẾ HOẠCH BÁN HÀNG
            </div>
            <div class="box-step-title khsx">
                LÊN KẾ HOẠCH SẢN XUẤT
            </div>
        </div>
    </div>
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track1.review.kehoachdetail', ['id' => $backId]) }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track1.review.kehoachdetail', ['id' => $nextId]) }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
