@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-buc-tranh-thanh-cong.png') }}" alt="">
    </div>
    <div class="review-detail-content kehoach-detail detail-3 ">
        <div class="review-flex-center">
            <div class="rows-3">
                <div class="box-label d-flex style-2">
                    <div class="box-label-icon">
                        <img src="{{ asset('img/carbon_forecast-hail.png') }}" alt="">
                    </div>
                    <p> Dự báo chính xác</p>
                </div>
                <div class="box-label d-flex style-2">
                    <div class="box-label-icon">
                        <img src="{{ asset('img/icon-park-outline_optimize.png') }}" alt="">
                    </div>
                    <p> Tối ưu hóa nguồn lực</p>
                </div>
                <div class="box-label d-flex style-2">
                    <div class="box-label-icon">
                        <img src="{{ asset('img/hugeicons_waste-restore.png') }}" alt="">
                    </div>
                    <p> Giảm lãng phí</p>
                </div>
            </div>


        </div>


    </div>
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track1.review.kehoachdetail', ['id' => $backId]) }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track1.review.sanxuat') }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
