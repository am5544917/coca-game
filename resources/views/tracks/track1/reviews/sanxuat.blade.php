@extends('layouts/review')
@section('track_number', 'track-2')
@section('content')
    <div class="box-title">
        <p>Sau khâu Lập Kế Hoạch, hãy cùng theo dõi những nội dung dưới đây để tìm hiểu về khâu “Sản xuất” và chuẩn bị sẵn cho mình những bí kíp thật xịn để tham gia các thử thách tại Chặng 1 nhé:
        </p>
    </div>
    <div class="rows-3 row-cards">
        <div class="row-item">
            <a href="{{route('tracks.track1.review.sanxuatdetail',['id' => 1])}}"> <img src="{{ asset('img/hologram-box-1.png') }}" alt=""></a>
        </div>
        <div class="row-item">
            <a href="{{route('tracks.track1.review.sanxuatdetail',['id' => 2])}}"><img src="{{ asset('img/hologram-box-2.png') }}" alt=""></a>
        </div>
        <div class="row-item">
            <a href="{{route('tracks.track1.review.sanxuatdetail',['id' => 3])}}"> <img src="{{ asset('img/hologram-box-3.png') }}" alt=""></a>
        </div>
    </div>
@endsection
