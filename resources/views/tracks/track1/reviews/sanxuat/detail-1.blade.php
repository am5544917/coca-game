@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-vai-tro-y-nghia.png') }}" alt="">
    </div>
    <div class="review-detail-content">
        <div class="review-flex-center">
            <div class="box-label">
                <p>Kiểm soát và tối ưu hóa toàn bộ quy trình sản xuất từ chuyển hóa nguyên liệu thô thành thành phẩm, đảm bảo chất lượng sản phẩm và tối ưu chi phí sản xuất.</p>
            </div>
      
        </div>


    </div>
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track1.review.sanxuat') }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track1.review.sanxuatdetail', ['id' => $nextId]) }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
