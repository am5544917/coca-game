@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-chuoi-gia-tri.png') }}" alt="">
    </div>
   
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track1.review.sanxuatdetail', ['id' => $backId]) }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track1.review.sanxuatdetail', ['id' => $nextId]) }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
