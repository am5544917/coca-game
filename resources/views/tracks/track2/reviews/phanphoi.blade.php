@extends('layouts/review')
@section('track_number', 'track-2')
@section('content')
    <div class="phanphoi-index">
        <div class="box-title">
            <p> Hãy cùng tìm hiểu những thông tin dưới đây trước khi chinh phục các thử thách tại khâu <br> Phân Phối tại
                Thị
                Trấn Hạnh Phúc nhé:
            </p>
        </div>
        <div class="box-label phanphoi-box-label">
            <p> Chuỗi phân phối có vai trò quản lý và điều phối toàn bộ quy trình lưu trữ và vận chuyển hàng hóa từ nhà
                máy/trung tâm phân phối đến các điểm bán hàng trên toàn quốc, trong đó gồm việc tối ưu hóa Phương thức lưu
                trữ,
                vận tải và mạng lưới phân phối nhằm đảm bảo hiệu quả, an toàn và dịch vụ khách hàng tốt nhất.</p>
        </div>
        <div class="rows-3">
            <div class="row-item phanphoi-label-item">
                <a href="{{ route('tracks.track2.review.phanphoidetail', ['id' => 1]) }}"><img
                        src="{{ asset('img/phanphoi-1.png') }}" alt=""></a>
            </div>
            <div class="row-item phanphoi-label-item">
                <a href="{{ route('tracks.track2.review.phanphoidetail', ['id' => 2]) }}"><img
                        src="{{ asset('img/phanphoi-2.png') }}" alt=""></a>
            </div>
            <div class="row-item phanphoi-label-item">
                <a href="{{ route('tracks.track2.review.phanphoidetail', ['id' => 3]) }}"><img
                        src="{{ asset('img/phanphoi-3.png') }}" alt=""></a>
            </div>
        </div>
        <div class="review-buttons">
            <div class="btn-back">
                <a href="{{ route('tracks.track2.review.index') }}">TRỞ LẠI</a>
            </div>
            <div class="btn-next">
                <a href="{{ route('tracks.track2.review.phanphoidetail', ['id' => 1]) }}"> TIẾP THEO</a>
            </div>
        </div>
    </div>





@endsection
