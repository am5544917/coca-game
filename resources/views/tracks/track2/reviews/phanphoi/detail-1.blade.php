@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-so-luoc-chuoi-khoi.png') }}" alt="">
    </div>
    <div class="review-detail-content phanphoi-detail">
        <div class="review-flex-center">
            <div class="box-label">
             <p>  Để phân phối sản phẩm đến đa dạng khách hàng trên toàn quốc, sản phẩm sau khi sản xuất sẽ được lưu trữ tại Kho thành phẩm của nhà máy hoặc các <span>Trung tâm phân phối</span>. Từ đó, sau khi đơn đặt hàng được xác nhận, sản phẩm sẽ được <span>phân phối theo 1 trong 2 kênh (Trực tiếp, Gián tiếp)</span>, ứng với các <span>tệp khách hàng khác nhau</span> như sau:</p>
            </div>
     
        </div>


    </div>
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track2.review.phanphoi') }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track2.review.phanphoidetail', ['id' => $nextId]) }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
