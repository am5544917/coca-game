@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-phoi-hop-cac-vai-tro.png') }}" alt="">
    </div>
    <div class="review-phanphoi-detail">
        <div class="box-label">
            <span>Để đảm bảo hoạt động phân phối sản phẩm của Thành phố Hạnh phúc hoạt động hiệu quả và tối ưu nhất, bộ phận Logistics đảm nhiệm và phối hợp nhiều chức năng quan trọng như:</span>
        </div>
    </div>
   
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track2.review.phanphoidetail', ['id' => $backId]) }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track2.review.phanphoidetail', ['id' => $nextId]) }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
