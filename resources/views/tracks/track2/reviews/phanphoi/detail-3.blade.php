@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-cac-yeu-to-thanh-cong.png') }}" alt="">
    </div>
    <div class="review-detail-content kehoach-detail detail-3 sanxuat-detail ">
    


    </div>
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track2.review.phanphoidetail', ['id' => $backId]) }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track2.review.phattrienthitruong') }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
