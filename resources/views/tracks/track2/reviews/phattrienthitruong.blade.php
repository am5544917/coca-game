@extends('layouts/review')
@section('track_number', 'track-2')
@section('content')
    <div class="phanphoi-index">
        <div class="box-title">
            <p> Sau khâu Phân phối với những kênh phát triển, phương tiện, mạng lưới Logistics thật đa dạng, hãy cùng tìm hiểu thêm về một khâu cực kỳ quan trọng giúp cho Thị Trấn mang những sản phẩm của mình tới thị trường một cách tốt nhất – khâu Phát triển thị trường:
            </p>
        </div>
        <div class="box-label phanphoi-box-label">
            <p>Hoạt động Phát triển thị trường xoay quanh 2 nhóm vai trò chính là <span>Thương mại</span> (Commercial) và <span>Bán hàng</span>(Sales), để đảm bảo việc phối hợp xây dựng chiến lược thương mại và thực thi bán hàng ở thị trường được triển khai hiệu quả nhất.</p>
        </div>
        <div class="rows-3">
            <div class="row-item phanphoi-label-item">
                <a href="{{ route('tracks.track2.review.phattrienthitruongdetail', ['id' => 1]) }}"><img
                        src="{{ asset('img/phanphoi-1.png') }}" alt=""></a>
            </div>
            <div class="row-item phanphoi-label-item">
                <a href="{{ route('tracks.track2.review.phattrienthitruongdetail', ['id' => 2]) }}"><img
                        src="{{ asset('img/phanphoi-2.png') }}" alt=""></a>
            </div>
            <div class="row-item phanphoi-label-item">
                <a href="{{ route('tracks.track2.review.phattrienthitruongdetail', ['id' => 3]) }}"><img
                        src="{{ asset('img/phanphoi-3.png') }}" alt=""></a>
            </div>
        </div>
        <div class="review-buttons">
            <div class="btn-back">
                <a href="{{ route('tracks.track2.review.index') }}">TRỞ LẠI</a>
            </div>
            <div class="btn-next">
                <a href="{{ route('tracks.track2.review.phattrienthitruongdetail', ['id' => 1]) }}"> TIẾP THEO</a>
            </div>
        </div>
    </div>





@endsection
