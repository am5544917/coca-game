@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-thuong-mai.png') }}" alt="">
    </div>
    <div class="review-detail-content">
        <div class="review-flex-center">
            <div class="box-label">
                <p>Hoạt động thương mại của chúng ta thống nhất và gắn chặt với định hướng của OU, người đóng vai trò xương sống trong việc: </p>
                   <p> •Phát triển Nhận diện thương hiệu</p>
                    <p>•Truyền thông và quảng bá nhãn hàng</p>
                   <p> •Chiến lược đổi mới/ phát triển nhóm sản phẩm</p>
            </div>
      
        </div>


    </div>
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track2.review.phattrienthitruong') }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track2.review.phattrienthitruongdetail', ['id' => $nextId]) }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
