@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-ban-hang.png') }}" alt="">
    </div>
    <div class="review-detail-content">
        <div class="review-flex-center">
            <div class="box-label">
                <p>Đội ngũ bán hàng đóng vai trò như “lực lượng tiền tuyến”, là cầu nối giữa Thành phố Hạnh phúc và thị
                    trường, giúp chúng ta mang sản phẩm đến khách hàng, từ đó hiện thực hóa các mục tiêu về tăng trưởng
                    doanh thu và thống lĩnh thị trường bằng cách: thực thi các chiến lược và chương trình thương mại, xây
                    dựng và quản lý mạng lưới khách hàng, và thúc đẩy số bán thực tế trên thị trường qua đa dạng cách kênh
                    bán, kênh phân phối khác nhau.</p>
            </div>

        </div>


    </div>

    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track2.review.phattrienthitruongdetail', ['id' => $backId]) }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track2.review.phattrienthitruongdetail', ['id' => $nextId]) }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
