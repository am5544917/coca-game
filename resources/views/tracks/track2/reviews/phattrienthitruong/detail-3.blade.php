@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-cac-chi-so-thanh-cong.png') }}" alt="">
    </div>
    <div class="review-detail-content kehoach-detail detail-3 sanxuat-detail ">
        <div class="review-flex-center">
            <div class="rows-3">
                <div class="box-label d-flex style-2">
                    <div class="box-label-icon">
                        <img src="{{ asset('img/fluent_autosum-16-filled.png') }}" alt="">
                    </div>
                    <p> Tổng năng suất chuỗi cung ứng</p>
                </div>
                <div class="box-label d-flex style-2">
                    <div class="box-label-icon">
                        <img src="{{ asset('img/icon-park-outline_manual-gear.png') }}" alt="">
                    </div>
                    <p> Hiệu quả hoạt động dây chuyền</p>
                </div>
                <div class="box-label d-flex style-2">
                    <div class="box-label-icon">
                        <img src="{{ asset('img/healthicons_chart-death-rate-increasing.png') }}" alt="">
                    </div>
                    <p> Tỉ lệ sử dụng nước & năng lượng</p>
                </div>
            </div>


        </div>


    </div>
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track2.review.phattrienthitruongdetail', ['id' => $backId]) }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track3.review.index') }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
