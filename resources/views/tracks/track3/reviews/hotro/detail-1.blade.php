@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-bo-phan-tai-chinh.png') }}" alt="">
    </div>
    <div class="review-detail-content">
        <div class="review-flex-center">
            <div class="box-label">
                <p>Tại thành phố hạnh phúc, bộ phận Tài chính có vai trò quản lý nguồn vốn, cơ cấu vốn, kế toán, các quyết định đầu tư, và cung cấp thông tin tài chính hỗ trợ cho việc ra quyết định. Hãy nhấp vào để mở khóa thông tin mô tả của 6 chức năng sau đây cho bộ phận Tài chính nhé:</p>
            </div>
      
        </div>


    </div>
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track3.review.index') }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track3.review.hotrodetail', ['id' => $nextId]) }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
