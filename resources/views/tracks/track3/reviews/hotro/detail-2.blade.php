@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-bo-phan-it.png') }}" alt="">
    </div>
    <div class="review-detail-content">
        <div class="review-flex-center">
            <div class="box-label">
                <p>Bộ phận Công nghệ Thông tin (IT) đóng vai trò với các phòng ban, mang đến giải pháp công nghệ tân tiến và hỗ trợ hoạt động toàn diện cho hoạt động kinh doanh.</p>
            </div>

        </div>


    </div>

    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track3.review.hotrodetail', ['id' => $backId]) }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track3.review.hotrodetail', ['id' => $nextId]) }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
