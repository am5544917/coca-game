@extends('layouts/review')
@section('content')
    <div class="review-tag-label">
        <img src="{{ asset('img/tab-label-bo-phap-ly.png') }}" alt="">
    </div>
    <div class="review-detail-content kehoach-detail detail-3 sanxuat-detail ">
     


    </div>
    <div class="review-buttons">
        <div class="btn-back">
            <a href="{{ route('tracks.track3.review.hotrodetail', ['id' => $backId]) }}">TRỞ LẠI</a>
        </div>
        <div class="btn-next">
            <a href="{{ route('tracks.track3.review.hotrodetail',['id' => $nextId]) }}"> TIẾP THEO</a>
        </div>
    </div>
@endsection
