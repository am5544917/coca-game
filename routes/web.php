<?php

use Illuminate\Support\Facades\Route;
Route::get('/', function () {
    return view('welcome');
});
Route::prefix('tracks')->as('tracks.')->namespace('App\Http\Controllers\Tracks')->group(function() {
    Route::prefix('track-1')->namespace('Track1')->as('track1.')->group(function() {
        Route::prefix('review')->controller('ReviewController')->as('review.')->group(function() {
            Route::get('/','index')->name('index');
            Route::get('/overview','overview')->name('overview');
            Route::prefix('ke-hoach')->group(function() {
                Route::get('/','kehoach')->name('kehoach');
                Route::get('/{id}','kehoachdetail')->name('kehoachdetail');
            });
            Route::prefix('san-xuat')->group(function() {
                Route::get('/','sanxuat')->name('sanxuat');
                Route::get('/{id}','sanxuatdetail')->name('sanxuatdetail');
            });
          
         
        });
    });
    Route::prefix('track-2')->namespace('Track2')->as('track2.')->group(function() {
        Route::prefix('review')->controller('ReviewController')->as('review.')->group(function() {
            Route::get('/','index')->name('index');
            Route::get('/overview','overview')->name('overview');
            Route::prefix('phan-phoi')->group(function() {
                Route::get('/','phanphoi')->name('phanphoi');
                Route::get('/{id}','phanphoidetail')->name('phanphoidetail');
            });
            Route::prefix('phat-trien-thi-truong')->group(function() {
                Route::get('/','phattrienthitruong')->name('phattrienthitruong');
                Route::get('/{id}','phattrienthitruongdetail')->name('phattrienthitruongdetail');
            });
        });
    });
    Route::prefix('track-3')->namespace('Track3')->as('track3.')->group(function() {
        Route::prefix('review')->controller('ReviewController')->as('review.')->group(function() {
            Route::get('/','index')->name('index');
            Route::get('/overview','overview')->name('overview');
            Route::prefix('ho-tro')->group(function() {
                Route::get('/','hotro')->name('hotro');
                Route::get('/{id}','hotrodetail')->name('hotrodetail');
            });
        });
    });
  
});